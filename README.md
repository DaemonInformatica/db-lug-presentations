# DB-LUG presentations

As a proud member of the Deb Bosch Linux UserGroup, I occasionally do presentations about facets and components of Linux that interest me and might be of use to others. This repository contains presentations and research material. 